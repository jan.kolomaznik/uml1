# Sekvenční diagram
Z termínu sekveční diagram, není tak jasné jeho určení jako z jeho alternativního názvu: Interakční diagram. Diagram se používá k popisu určitého typu interakcí mezi různými prvky v modelu. Tato interakce je součástí dynamického chování systému.

Toto interaktivní chování je v UML reprezentováno dvěma diagramy známými jako Sequence diagram a Collaboration diagram. Základní účel obou diagramů je podobný.

Sekvenční diagram klade důraz na časovou posloupnost zpráv a diagram spolupráce klade důraz na strukturální organizaci objektů, které odesílají a přijímají zprávy.

---
1.  Role modelu v analýze nebo návrhu
2.  Základní prvky sekvenčního diagramu
3.  Příklady v EA