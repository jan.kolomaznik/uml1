# Diagram komponent

Diagramy komponent se liší z hlediska povahy a chování. Diagramy komponent se používají k modelování fyzických aspektů systému. Nyní je otázkou, jaké jsou tyto fyzické aspekty? Fyzické aspekty jsou prvky, jako jsou spustitelné soubory, knihovny, soubory, dokumenty atd., které jsou umístěny v uzlu.

Diagramy komponent se používají k vizualizaci organizace a vztahů mezi komponentami v systému. Tyto diagramy se také používají k vytváření spustitelných systémů.

---
1.  Role modelu v analýze
2.  Základní prvky diagramu aktivit
3.  Příklady v EA