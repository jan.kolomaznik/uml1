Analytické digramy tříd a objektů oředstavují jeden z prvních diagramů analýzy.

Analytický digram tříd představuje naši představu o systému. Jedná se tedy o abstrakci problémové domény. Měli by mapovat pojmy zkutečného světa.

Za problémovou doménupovažujeme doménu, ze které vzešel první podmět  pro vznik systému. Obvykle jde o specifickou obchodní oblast.
To je hlavní orzíl od návrhových diagramů tříd, které pravěpodobě znáte. Ty jsou většinou uzce svázavny s jazykem nebo pgormaovou platformou, pro kterou se systém vyvyjí. 
analytický model se tedy zaměruje na to _co_ musá systém dělat, ale nezabývá se aspektem jak to udělá.
   
----
1.  Role modelu v analýze
2.  Základní prvky diagramu tříd
3.  Základní prvky diagramu objektů
4.  Příklady v EA
5.  Vlastní práce účastníků