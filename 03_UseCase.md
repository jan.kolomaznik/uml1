# Model případů užití

**Diagram případů užití** nebo **use case diagram** (**UC diagram**) je v [softwarovém inženýrství](https://cs.wikipedia.org/wiki/Softwarov%C3%A9_in%C5%BEen%C3%BDrstv%C3%AD "Softwarové inženýrství") jeden z [diagramů](https://cs.wikipedia.org/wiki/Diagram "Diagram") chování definovaných v [UML](https://cs.wikipedia.org/wiki/Unified_Modeling_Language "Unified Modeling Language") a [SysML](https://cs.wikipedia.org/w/index.php?title=Systems_Modeling_Language&action=edit&redlink=1 "Systems Modeling Language (stránka neexistuje)").

Diagram případů užití zachycuje vnější pohled na modelovaný systém a tím pomáhá odhalit hranice systému a slouží jako podklad pro odhady rozsahu. Jde o posloupnost souvisejících transakcí mezi účastníkem (zpravidla uživatelem v určité roli, ale také jiným systémem) a systémem během vzájemného dialogu. Hlavním účelem je zachycení aktérů, kteří se systémem komunikují a vztahů mezi službami a těmi, kterým jsou poskytovány, a to vizuální i textovou podobou, která je srozumitelná vývojářům systému i zákazníkům (tj. těm, kteří jej mají používat).

Používá elementů:

-   **Případ užití** (značený oválně) – posloupnost akcí ve vztahu s aktéry, může obsahovat vazby
    -   Include – případ užití může obsahovat jiný (Zahrnující – např.: Editovat text → Psát text, Vložit obrázek, atd.)
    -   Extend – případ užití může rozšiřovat jiný (Rozšiřující – např.: Otevřít dokument → Import z jiného formátu, atd.)
    -   [Generalization](https://cs.wikipedia.org/wiki/Generalizace "Generalizace") – případ užití může být speciálním případem jiného
-   **Aktér/účastník** (značený figurou) – popis externích objektů vstupujících do vztahu s procesy, může obsahovat vazby
    -   Generalization

![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Restaurant_Model.png/583px-Restaurant_Model.png?20090330145938)

----
1.  Role modelu v analýze
2.  Základní prvky modelu případů užití
3.  Příklady v EA
4.  Vlastní práce účastníků