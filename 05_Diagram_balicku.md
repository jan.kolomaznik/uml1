# Model balíčků

Package diagram, druh strukturálního diagramu, ukazuje uspořádání a organizaci prvků modelu ve středním až velkém měřítku projektu. Diagram balíku může ukazovat jak strukturu, tak závislosti mezi podsystémy nebo moduly, zobrazující různé pohledy na systém, například jako vícevrstvá (neboli vícevrstvá) aplikace - vícevrstvý aplikační model.

----
1.  Základní prvky diagramu balíčků
2.  Debata o roli pro uspořádání analýzy a projektu
3.  Ukázka v EA