# Demo plantUML

Link: https://plantuml.com/

```plantuml
class Dummy {
  String data
  void methods()
}

class Flight {
   flightNumber : Integer
   departureTime : Date
}


Dummy <|-- Flight
```

