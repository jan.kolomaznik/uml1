# Vývoj SW systémů, vývojové fáze SW projektu, modely apod


Definice softwarového inženýrství (SW) považováno za velmi metodickou disciplínu, která sleduje několik základních cílů: 
1. maximálně **zjednodušit** tvorbu software, 
2. zavedení **automatizace** do tvorby software, 
3. zlepšování **spolehlivosti** software, 
4. zavádět do tvorby software **inženýrské přístupy**, 
5. postupná orientace na tvorbu složitých **programových systémů** a její zabezpečení, 
6. reagovat na využití **nejnovějších informačních technologií** pro zpracování informací, 
7. orientace na **urychlení vývoje** software a efektivního řízení vývoje.

Přerod od jednoduchých programů k programovým systémům, vč. dynamických komponent.

## Historie SWI
- Začátky zpracování informací 50. léta 20. století, kybernetika
- 50.–70. léta 20. století, Výzkumný ústav matematických strojů (VÚMS)
- 60. léta 20. století, překotný vývoj bez koncepce, softwarové projekty mnohdy nedokončeny, neřešila se údržba softwaru -> **softwarová krize**
	- Programy tvořil každý kdo uměl programovat, ale bez rozmyslu, v UI skoro každý program co dokázal rozhodovat označen jako ES
	- Software byl mnohdy příčinou havárie (absence testů)
- Na rozvoj disciplíny SWI měl vliv i Moorův zákon – „**Výkon hardwaru vzrůstá zhruba dvakrát za dva roky**“, tj. překotný vývoj softwaru, nutno nastolit koncepci **vývoje** (vedení vývoje), **testování**, **údržby**
### Softwarové havárie
- **Sonda Mariner I** (1962), špatně aplikován vzorec v řídícím počítači rakety programátorem, sonda zničena po 293 s
- **Sonda Mercury (1962)**, Fortran záměna čárky za tečku, cyklus nepoužit, odhaleno
- **Apollo 11 (1969)**, přistávací modul se o 4 s odchýlil od plánované trajektorie, příčina nevypnutý radar, neplánovaná spotřeba procesorového času
- **Raketa Ariane 5 (1996)**, neobsloužená výjimka v pohyblivé řadové čárce, špatná obsluha trysky, 500 mil. $
- **Přistávací modul na Marsu (1999)**, problém v komunikace mezi rozhraními (km/míle), ztráta 125 mil. $
- **Y2K (2000)**, rok ukládán skrze úsporu místa na dvou pozicích, tedy 2000 stejné jako 1900. Byť se s přechodem roku automaticky generovaly automaticky nějaké sestavy (pozvánky do první třídy pro stoleté, atd.), skrze miliardy dolarů investované do upgradu systému katastrofické vize nenastaly, investice stovky miliard $ (prevence) https://www.euro.cz/byznys/dezinsekce-za-ctvrt-bilionu-dolaru-816743
- **Výpadek elektřiny USA (2003)**, neregistrovaný výpadek automatizovaného hlásiče poruch, Niagarská elektrárna, 50mil.obyvatel, zemřeli 3 lidé, 6mld.$

### Reakce na SW krizi
- Nutnost eliminovat rizika spjatá s tvorbou softwaru a jeho nasazením! **Vývojové metodiky, čitelný kód, dokumentace softwaru, testování softwaru, znovupoužitelný kód.**
 
- NATO 1968, SRN, konference na téma Softwarové inženýrství, definovány směry vývoje, definice SWI: 
	*Softwarové inženýrství je disciplína, která se zabývá zavedením a používáním řádných inženýrských principů do tvorby software tak, abychom dosáhli ekonomické tvorby software, který je spolehlivý a pracuje účinně na dostupných výpočetních prostředcích*

- Uvedené technické problémy související se softwarovou krizí vedly k přerodu spartanského vývoje inženýrskou disciplínu v tvorbě softwaru
- 70. léta, základní formalizace principů oboru SWI
- Vzniká první generace nástrojů pro podporu této disciplíny, které jsou označovány jako CASE nástroje (Computer Aided Software Engineering)

## Oblasti SWI
- Správa požadavků (Software requirements)
- Softwarový návrh (Software design)
- Tvorba softwaru (Software construction)
- Testování softwaru (Software testing)
- Údržba softwaru (Software maintenance)
- Správa konfigurací (Software configuration management)
- Řízení vývoje (Software engineering management)
- Softwarový proces (Software process)
- Nástroje a metody softwarového inženýrství (Software engineering tools and methods)
- Kvalita softwaru (Software quality)

## Softwarový projekt
Projekt je dobře definovaná posloupnost činností, která je zaměřena na dosažení nejistého cíle, má určen začátek a konec, a je uskutečňována pomocí zdrojů
- lidí
- dostupných prostředků

- Projekt je specifická nerutinní akce, která vyžaduje plánování (manažerský přístup)
- Čím složitější je projekt, tím více vyžaduje plánování 

**Projektové řízení v mezích zvolené metodiky (SCRUM, UP, …) a jiných osvědčených přístupů (fragmentů metodik)**

Doposud nevytvořen referenční popis softwarového procesu, jako základ považován vodopádový model (analýza, návrh, implementace, testování a údržba) 

![](http://szz.g6.cz/lib/exe/fetch.php?cache=&w=774&h=538&media=temata:30-zivotni_cyklus_softwaru:vodopadovy_model.jpeg)

Omezení je v prodlevách, výsledek závislý na specifikaci požadavků, nelze odhalit kvalitu před zhotovením
- modifikace tohoto přístupu, např. (R)UP 

### Rozdílnost oproti klasickým produktům?

![](https://upload.wikimedia.org/wikipedia/commons/0/0c/GoldenGateBridge-001.jpg)

- softwarový produkt je vyvíjen iteračním způsobem
- jsou spravovány požadavky na něj kladené
- využívá se již existujících softwarových komponent
- model softwarového systému je vizualizován
- průběžně je ověřována kvalita produktu
- změny systému jsou řízeny

![](https://i.iinfo.cz/images/482/vyvoj-software-houpacka.png)

## Úskalí spatného návrhu SW
Projekt bez vhodného návrhu může v prvopočáteční fázi být akcelerován (funkcionalita), tj. lepší poměr času k dodané funkcionalitě, avšak po dosažení bodu zvratu (design pay – off line), zde již převažuje povaha kvalitního návrhu.

![](https://martinfowler.com/bliki/images/designStaminaGraph.gif)

Průběh výboje SW je charakterizován častými změnami
- SW nelze navrhnout v jednom kroku shora dolů, v tomto přístupu selhává vodopádový přístup, spíše vhodný interaktivní přístup (zpřesňování)

![](https://profinit.eu/wp-content/uploads/2019/05/blog5.png)

- Dobrá přístup k vývoji SW umožňuje agilní přístup.
- Použití agilních metodik a spirálového vývojového modelu.

![](https://miro.medium.com/max/800/0*wD_Z9w3kn-FqfKoc.jpg)


# UML a UP
Jak do toho všeho zapadá UML a co je to UP?

## Co je to UML?
Unified Modelling Language (UML) je univerzální jazyk pro vizuální modelování systémů
- Podporuje všechny životní cykly
- Mohou jej implementovat všechny nástroje CASE

Na základě zkušeností sjednotil modelovací metodiky, nejčistší řešení tvorby jazyku pro vizuální modelování a proces softwarového inženýrství

UML není metodika!
- UML je vizuální modelovací jazyk
- UP je metodika

## (R)UP metodika

*Metodika je ucelený systém metod, technik, nástrojů a filosofického zdůvodnění **KDO, CO, KDE, JAK a PROČ** má udělat, aby se vytvořil software, který komputerizuje požadavky zákazníka na aktivity a zpracování dat v zadané problémové doméně.*

- **Metodika UP** se člení na fáze (Zahájení, Rozpracování, Konstrukce, Zavedení) v kterých se aplikují osvědčené pracovní založené na vhodných metodách, technikách a nástrojích.  
- **Jazyk UML** využit zakladateli UP pro deskripci jednotlivých fází.

### KDO, CO, KDY, JAK a PROČ 
- Pod **KDO** vidí UP dělníka (worker) a popisuje jeho roli jak jako jedince, tak i ve vývojovém týmu.
- Pod **CO** se myslí „úkoly, které jsou uděleny jednotlivcům a týmům“.
- Pod **KDY** se rozumí „v následnosti pracovních postupů jednotlivých fází“. To jsou vlastně posloupnosti jistých aktivit, které jsou ve fázích předepsány.
- Pod **JAK** se rozumí „způsob provádění aktivit a získání tzv. artefaktů (vstupy a výstupy aktivit)“
- Slovíčko **PROČ** je zdůvodněno celkovou filosofií metodiky jak dosáhnout předepsaných cílů v pracovních postupech a celých fázích.

### Tři axiomy UP
1. Řízení vývojářských prací je výlučně na základě řízení případem užití a projektovým rizikem.
2. Pozornost vývoje je soustředěna na architekturu cílového software.
3. Model životního cyklu je iterativní a přírůstkový (inkrementální).

- **Případy užití**, které reprezentují požadavky uživatele na vyvíjený software, jsou vlastně jak počátečním, tak i dále používaným **supervizorem** vývoje software. 
- UP posuzuje vývoj software v ustanoveném projektu na základě **analýzy rizik**, které mohou nastat a značně narušit uspokojivý vývoj software. Riziky se zabývá **projektový manažer,** rizika nutné minimalizovat. 
- **Vývoj software** je metodikou UP řešena postupně, na základě výběru modelu, který je vhodný (např. komponentový vývoj) pro rozklad robustného softwarového systému. 
- **Iterativnost** znamená rozklad projektu na menší části – iterace, které usnadní postupnou cestu vedoucí k cílovému software. Pro každou iteraci se uplatňují pracovní postupy.

### Iterativní vývoj
Podstata metodiky UP je založena na **iteracích a přírůstcích**. Projekt je členěn na dílčí mini projekty (iterace), protože je pohodlnější a lépe zvládnutelná malá dílčí část. 

**Pro každou iteraci** se vlastně provede **pět** základních **pracovních postupů** (workflows):
- **Požadavky**, zde vznikají specifikace požadavků zákazníka, které on přiřazuje softwarovému systému jako nutnou podmínku jeho nákupu a implementaci ve stanoveném prostředí.
- **Analýza**, vyhodnocení požadavků a jejich přerod do funkcionality softwarového systému.
- **Návrh**, odraz požadavků v architektuře softwarového systému.
- **Implementace**, uskutečnění tvorby software.
- **Testování**, verifikace toho, že implementace funguje tak, jak je v požadavcích zákazníka stanoveno.

![](media/faze_UP.png)

V jedné fázi může proběhnout více iterací. Pracovní postup může být přes dvě fáze projektu. Nevylučuje se i souběh iterací, byť se předpokládá jejich přímá návaznost. 

### Struktura metodiky UP
- ZAHÁJENÍ (inception) období plánování
- ROZPRACOVÁNÍ (elaboration) období návrhu
- KONSTRUKCE (construction) počátky provozuschopnosti cílového software
- ZAVEDENÍ (transition) nasazení software do uživatelského prostředí. 

*Pracovní postupy přesahují jednotlivé fáze!*
- Ve fázi ZAHÁJENÍ je nejvíce času věnováno pracovním postupům sestavení požadavků a jejich analýze.
- Ve fázi ROZPRACOVÁNÍ je silná orientace na požadavky, analýzu a částečně na návrh.
- Ve fázi KONSTRUKCE je důraz jednoznačně kladen na návrh a implementaci.
- Ve fázi ZAVEDENÍ se dokončuje implementace a testování.

### Fáze ZAHÁJENÍ
**Souhrnné cíle** 
Tato fáze je startovací fází metodiky a od toho závisí obsah všech plánovacích bodů (Souhrnné cíle, Primární zaměření a Milník). 

#### Za Souhrnné cíle jsou stanoveny:
- Tvorba podmínek proveditelnosti (Feasibility conditions). 
- Nadnesení obchodního (podnikatelského) případu (obchodní přínos).
- Zachycení podstatných požadavků (rozsah vznikajícího systému).
- Označení kritických rizik. 

#### Hlavní pracovníci:
- V této fázi čeká nejvíce práce **manažera projektu**, jímž by měl být **informační manažer** žádající organizace. Je zde spolupodíl **systémového projektanta**. Na analýze požadavků zákazníka podílí **zákazník** sám, budoucí uživatelé, testeři.

 ![](media/Faze_zahajeni.png)

#### Primární zaměření
- Ve fázi dominují pracovní postupy zabývající se specifikací požadavků a jejich analýzou.
- Vedle toho je důraz kladen na některé návrhářské a implementační práce.
- Neměla by se rovněž opomíjet kontextová analýza zadavatele a souvislost s jeho požadavky.
- Velmi důležité je stanovit celkový model životního cyklu vyvíjeného software. 

#### Milník
Milníkem fáze je předmět životního cyklu a rozsah softwarového systému (Life Cycle Objectives). Je doporučována následující verifikační tabulka

### Fáze ROZPRACOVÁNÍ
Fáze velmi důležitá, závislá na předchozím vstupu (fázi), je kritická pro následující fázi KONSTRUKCE. Chyby zde vzniklé se mohou nepříznivě odrazit na koncepci software zadavatele!

#### Souhrnné cíle
- Tvorba spustitelného architektonického základu
- Vylepšení odhadu rizik
- Definice atributů kvality
- Zachycení případů užití pro 80 % funkčních požadavků
- Tvorba přesného plánu fáze KONSTRUKCE
- Formulace nabídky, která zahrnuje veškeré prostředky, čas, vybavení, personál a náklady. 

#### Hlavní pracovníci 
Podíl **zadavatele**, který poskytuje interview k vysvětlení požadavků a prostředí pro nasazení cílového software. Práce **analytiků** a **programátorů** softwarové firmy, která má cílový software produkovat.

![](media/Faze_rozpracovani.png)

#### Primární zaměření
- V pracovním postupu Požadavky: Upřesnění rozsahu systému a požadavků na něj kladených
- V pracovním postupu Analýza: Upřesnění toho, co budeme tvořit
- V pracovním postupu Návrh: Tvorba logické architektury cílového software
- V pracovním postupu Implementace: Tvorba Spustitelného architektonického základu
- V pracovním postupu Testování: Testování korektnosti Spustitelného architektonického základu

#### Poznámka
Důraz je kladen na pracovní postupy Požadavky, Analýza a Návrh. Pracovní postup Implementace se zvýrazňuje až koncem fáze ROZPRACOVÁNÍ, kdy se začíná pracovat na Spustitelném architektonickém základu.

### Fáze KONSTRUKCE 
#### Souhrnné cíle
- Zde vzniká reálná nutnost splnit všechny požadavky Analýzy a Návrhu a vyvinout z dříve připraveného Spustitelného architektonického základu cílový software. Klíčovým heslem fáze KONSTRUKCE je důsledně zachovat integritu dříve navržené architektury (neporušit ji).
- Hlavním procesem konstrukce je implementace. Narůstá počet testů, realizují se jak dílčí, tak integrační testy.

#### Hlavní pracovníci
Analytici a programátoři softwarové firmy.

#### Primární zaměření
- Ve fázi KONSTRUKCE se mají pracovní kroky Požadavky, Analýza, Návrh, Implementace a testování své speciální dominantní cíle. 
- Primární zaměření uvádí následující seznam cílů:

![](media/Faze_konstrukce.png)

#### Primární zaměření
- V pracovním kroku Požadavky – odhalit všechny požadavky zadavatele, které byly dosud opomenuty
- V pracovním kroku Analýza – dokončit analytický model (statický a dynamický model problémové domény)
- V pracovním kroku Návrh - dokončit model návrhu a prověřit zachování integrity architektury
- V pracovním kroku Implementace - zajistit počáteční provozní způsobilost (Initial Operational Capability) dosud vytvořeného software, tj. Počáteční funkční variantu
- V pracovním kroku Testování – testovat počáteční funkční variantu software

#### Milník
- Výsledkem fáze KONSTRUKCE je to, že je připravena Počáteční funkční varianta softwarového systému pro testování na počítačích zadavatele.

### Fáze ZAVEDENÍ
Nastane-li okamžik, kdy je dokončeno testování a konečné nasazení systému, potom začíná fáze ZAVEDENÍ (transition). V podstatě tato fáze zahrnuje opravu všech chyb v otestované verzi software (v tzv. Beta verzi) a skutečné přenesení software na všechny počítače zadavatele.

#### Souhrnné cíle
- Oprava chyb
- Příprava pracoviště zadavatele k přijetí software
- Nutné přizpůsobení software tak, aby korektně fungoval na pracovištích zadavatele
- Řešení neočekávaných problémů a evokovaná úprava software
- Tvorba uživatelských manuálů a další dokumentace stanovené v projektu
- Konzultace se zadavatelem a současně prvním uživatelem
- Konečná revize celého projektu vývoje software.

![](media/Faze_zavedeni.png)

#### Hlavní pracovníci
Projektový manažer, systémový integrátor, analytici a programátoři softwarové firmy; testeři.

#### Primární zaměření
Důraz na pracovní kroky Implementace a Testování, testy by měl odhalit dílčí chyby a chyby integrace, pokud se vrací k Analýze, projekt je nekvalitně navržen. Hlavní aspekty:
- Úprava návrhu, jsou-li během testování odhaleny chyby v návrhu software.
- V pracovním kroku Implementace – přizpůsobení software pracovišti uživatele a oprava chyb, které nebyly při testování nalezeny
- V pracovním kroku Testování – provést beta testy a přejímací testy na pracovišti uživatele.

#### Milník
Milníkem fáze je nasazení - uvolnění softwarového produktu (Product Release). To vyžaduje ukončení beta testů, přejímacích testů, opravy případných chyb a produkt je uvolněn a přijat do užívání. Tomu také odpovídají verifikační podmínky milníku.

### Shrnutí
Text je základním přehledem nejen vlastností metodiky UP, ale rovněž souhrnných cílů, pracovníků, primárního zaměření a milníků. Jsou použity podrobné tabulky, jejich využití je pro postup vývoje cílového software zásadní. 

# Computer Aided Software Engineering (CASE)
V překladu znamená _počítačem podporované [softwarové inženýrství](https://cs.wikipedia.org/wiki/Softwarov%C3%A9_in%C5%BEen%C3%BDrstv%C3%AD "Softwarové inženýrství")_ nebo _vývoj software s využitím počítačové podpory_. 

Jde o použití [softwaru](https://cs.wikipedia.org/wiki/Software "Software") pří vývoji (resp. údržbě) [počítačových programů](https://cs.wikipedia.org/wiki/Po%C4%8D%C3%ADta%C4%8Dov%C3%BD_program "Počítačový program"), za účelem dosáhnutí vyšší kvality, bezchybnosti, udržovatelnosti apod. Podpora může probíhat v různých stádiích životního cyklu programu – při sběru požadavků, analýzách, návrhu, [programování](https://cs.wikipedia.org/wiki/Programov%C3%A1n%C3%AD "Programování") (s pomocí [IDE](https://cs.wikipedia.org/wiki/V%C3%BDvojov%C3%A9_prost%C5%99ed%C3%AD "Vývojové prostředí")) atd. Nástroje používané pro tento účel se obvykle nazývají **CASE nástroje**. Některé z nich slouží k automatizaci vývojového procesu.

CASE nástroje primárně umožňují modelování IT systému pomocí diagramů (člověk lépe chápe obrázek než složitě psané slovo), generování zdrojového kódu z modelu (reverse engineering), zpětné vytvoření modelu podle existujícího zdrojového kódu (usnadňuje práci programátorům), synchronizaci modelu a zdrojového kódu, vytvoření dokumentace z modelu. Některé obvyklé funkce CASE nástrojů:

-   generování [zdrojového kódu](https://cs.wikipedia.org/wiki/Zdrojov%C3%BD_k%C3%B3d "Zdrojový kód")
-   [datové modelování](https://cs.wikipedia.org/wiki/Datov%C3%A9_modelov%C3%A1n%C3%AD "Datové modelování")
-   [objektově orientovaná analýza a design](https://cs.wikipedia.org/wiki/Objektov%C4%9B_orientovan%C3%A1_anal%C3%BDza_a_design "Objektově orientovaná analýza a design") v [UML](https://cs.wikipedia.org/wiki/Unified_Modeling_Language "Unified Modeling Language"), [BPMN](https://cs.wikipedia.org/wiki/BPMN "BPMN"), [ArchiMate](https://cs.wikipedia.org/wiki/ArchiMate "ArchiMate") atd.
-   [refaktorování](https://cs.wikipedia.org/wiki/Refaktorov%C3%A1n%C3%AD "Refaktorování")
-   správa konfigurací ([konfigurační řízení](https://cs.wikipedia.org/w/index.php?title=Konfigura%C4%8Dn%C3%AD_%C5%99%C3%ADzen%C3%AD&action=edit&redlink=1 "Konfigurační řízení (stránka neexistuje)"))

CASE nástroje jsou navrženy tak, aby podporovaly týmovou práci při vývoji systému, zajišťují sdílení rozpracovaných fragmentů, správu vývoje, sledují konzistenci modelu systému, automatizují některé procesy, hlídají dodržování zvolené metodiky, některé umožňují řízení celého [životního cyklu aplikací](https://cs.wikipedia.org/wiki/%C5%BDivotn%C3%AD_cyklus_informa%C4%8Dn%C3%ADho_syst%C3%A9mu "Životní cyklus informačního systému"). Úspěch využití CASE nástrojů záleží mimo jiné na vybrané metodice.

## Příklady CASE nástrojů

-   Visual Paradigm (Visual Paradigm)
-   [Enterprise Architect](https://cs.wikipedia.org/wiki/Enterprise_Architect "Enterprise Architect") (Sparx Systems)
-   MagicDraw (No Magic)
-   [Powerdesigner](https://cs.wikipedia.org/w/index.php?title=Powerdesigner&action=edit&redlink=1 "Powerdesigner (stránka neexistuje)") (Sybase)
-   Oracle Designer (Oracle)
-   Case Studio
-   [Rational Rose](https://cs.wikipedia.org/w/index.php?title=Rational_Rose&action=edit&redlink=1 "Rational Rose (stránka neexistuje)")
-   [Select Architect](https://cs.wikipedia.org/wiki/Select_Architect_(software) "Select Architect (software)")
-   [MS Visio](https://cs.wikipedia.org/wiki/Microsoft_Visio "Microsoft Visio")
-   Moon Modeler - [(Datensen)](https://www.datensen.com/)

