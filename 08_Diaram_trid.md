# Diagram tříd

Diagram tříd je statický diagram. Představuje statický pohled na aplikaci. Diagram tříd se nepoužívá pouze pro vizualizaci, popis a dokumentaci různých aspektů systému, ale také pro konstrukci spustitelného kódu softwarové aplikace.

Diagram tříd popisuje atributy a operace třídy a také omezení kladená na systém. Diagramy tříd jsou široce používány při modelování objektově orientovaných systémů, protože jsou jedinými diagramy UML, které lze přímo mapovat pomocí objektově orientovaných jazyků.

Diagram tříd zobrazuje kolekci tříd, rozhraní, přidružení, spolupráce a omezení. Je také známý jako strukturální diagram.

# Diagram objektů
Diagramy objektů jsou odvozeny z diagramů tříd, takže diagramy objektů jsou závislé na diagramech tříd a nebo naopak :-)

Diagramy objektů představují instanci diagramu tříd. Základní koncepty jsou podobné pro diagramy tříd a diagramy objektů. Objektové diagramy také představují statický pohled na systém, ale tento statický pohled je snímek systému v konkrétním okamžiku.

Diagramy objektů se používají k vykreslení sady objektů a jejich vztahů jako instance.
---
1.  Role modelu v analýze
2.  Základní prvky diagramu tříd
3.  Základní prvky diagramu objektů
4.  Příklady v EA
5.  Vlastní práce účastníků