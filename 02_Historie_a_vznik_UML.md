# Co je to UML

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/UML_logo.svg/440px-UML_logo.svg.png)
**UML**, **Unified Modeling Language** je v [softwarovém inženýrství](https://cs.wikipedia.org/wiki/Softwarov%C3%A9_in%C5%BEen%C3%BDrstv%C3%AD "Softwarové inženýrství") grafický jazyk pro [vizualizaci](https://cs.wikipedia.org/wiki/Vizualizace "Vizualizace"), [specifikaci](https://cs.wikipedia.org/wiki/Specifikace "Specifikace"), navrhování a dokumentaci programových systémů. UML nabízí standardní způsob zápisu jak návrhů systému včetně konceptuálních prvků jako jsou [business procesy](https://cs.wikipedia.org/w/index.php?title=Business_proces&action=edit&redlink=1 "Business proces (stránka neexistuje)") a systémové funkce, tak konkrétních prvků jako jsou příkazy [programovacího jazyka](https://cs.wikipedia.org/wiki/Programovac%C3%AD_jazyk "Programovací jazyk"), [databázová schémata](https://cs.wikipedia.org/w/index.php?title=Datab%C3%A1zov%C3%A9_sch%C3%A9ma&action=edit&redlink=1 "Databázové schéma (stránka neexistuje)") a znovupoužitelné programové komponenty.

UML podporuje objektově orientovaný přístup k analýze, návrhu a popisu programových systémů. UML neobsahuje způsob, jak se má používat, ani neobsahuje metodiku(y), jak analyzovat, specifikovat či navrhovat programové systémy.

# Historie

## 70. léta
- Vznik prvních objektově orientovaných jazyků 
- První objektově orientové metody anylýzy a návrhu
 
## Polovina 90. let
- Prudký nárůst jejich počtu (dle OMG99 v 1994 více než 50) 
	- Problémy s orientací v této oblasti
	- Problémy s nalezením vhodného nástroje Období označováno jako „Válka metod“
	- Dochází k částečné redukci, vzájemné slučování metod
	- Postupný zánik méně perspektivních metod 
- Začínají se objevovat metody, které zaujímají dominantní postavení 
	- Boochova (Booch’93)
	- Jacobsonova (OOSE – Object Oriented Software Engineering)
	- Rumbaughova (OMT-2 – Object Modeling Technique) Rumbaughova
- Ve všech případech ucelené metody objektově orientované analýzy a návrhu
	- každá měla své silné a slabé stránky.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/OO_Modeling_languages_history.jpg/1280px-OO_Modeling_languages_history.jpg)

## Vznik UML
Vývoj UML začal v roce 1994, kdy [[Grady Booch]] a [[Jim Rumbaugh]] začali ve firmě [Rational Software](https://cs.wikipedia.org/w/index.php?title=Rational_Software&action=edit&redlink=1 "Rational Software (stránka neexistuje)") (nyní součást firmy [IBM](https://cs.wikipedia.org/wiki/IBM "IBM")) spojovat své metodiky
- Booch a OMT ([Object Modeling Technique](https://cs.wikipedia.org/w/index.php?title=Object_Modeling_Technique&action=edit&redlink=1 "Object Modeling Technique (stránka neexistuje)")). V září 1996 do firmy Rational Software vstoupil [[Ivar Jacobson]] se svojí metodologii OMSE ([Object-Managed Software Engineering](https://cs.wikipedia.org/w/index.php?title=Object-Managed_Software_Engineering&action=edit&redlink=1 "Object-Managed Software Engineering (stránka neexistuje)")).
- Výsledkem jejich práce byl návrh UML (verze 0.9) a metodika RUP ([Rational Unified Process](https://cs.wikipedia.org/wiki/Rational_Unified_Process "Rational Unified Process")). 

### Zapojení velkých hráčů
- Během roku 1996 zpracovávání námětu veřejnosti
- UML Partners Consorcium
	- Rational Software Corporation
	- Digital Equipment Corp., 
	- Hewlett-Packard Company, 
	- IBM Corporation, 
	- Microsoft Corporation, 
	- Oracle Corporation

![](https://cdn-images.visual-paradigm.com/guide/what-is-uml/01-uml-history.png)

### UML 1.0
- vytvořená v roce 1997
- používá pro návrh, který poslala firma Rational Rose standardizační komisi. 

### UML 1.1
- vnikla ještě v témže roce jako UML 1.0 (1997)
- schválená Standardizační organizace OMG jako standard UML verze 1.1
- byli v ní začleněny prvky z dalších metodik

### UML 1.2
- vytvořená v roce 1998

### UML 1.3
- polovina roku 1999
- V rámci této verze UML prodělalo většími změnami

### UML 1.4
- v roce 2001

### UML 1.5
- v roce 2002

### UML 2.0
- od roku 2001 OMG připravovala verzi 2.0
- přinaší podstatná rozšíření
- text první části (SuperStructure) byl schválen na podzim 2004, ale ještě nebyla dokončena formální úprava dokumentu.

### UML 2.4.1
- formálně vydána v srpnu 2011

### UML 2.5
- byla vydána v říjnu 2012 jako „In progress“ verze a byla oficiálně vydána v červnu 2015.

### UML 2.5.1
- formalní verze 2.5.1 byla přijata v prosinci 2017, stejně jako OCL verze 2.4.
- [Mezinárodní organizací pro normalizaci](https://cs.wikipedia.org/wiki/Mezin%C3%A1rodn%C3%AD_organizace_pro_normalizaci "Mezinárodní organizace pro normalizaci") (ISO) tyto dokumenty přijala jako standard ISO/IEC 19505.

# Alternativy

## BPMN (Business Process Model and Notation)

![](https://gelwpprd.wpengine.com/wp-content/uploads/2022/06/bpmn-logo.jpg)

Grafický jazyk pro specifikaci obchodních procesů v diagramu obchodních procesů (BPD), tak aby byly procesy srozumitelné všem podnikovým uživatelům.

Známé také jako Business Process Modeling Notation. 
- Iniciativa Business Process Management Initiative (BPMI) 
- která je spravována skupinou Object Management Group (OMG) od sloučení obou organizací v roce 2005. 
- Od ledna 2011 je aktuální verze BPMN 2.0.

Ústředním rozdílem mezi UML a BPMN je to, že UML je objektově orientovaný, zatímco BPMN využívá procesně orientovaný přístup, který je vhodnější v rámci domény obchodních procesů.

![](https://sparxsystems.com/enterprise_architect_user_guide/14.0/images/business-process-diagram-with-lanes-8364.png)

# ERD a DFD
Modely DFD a ERD oba ukazují prezentace dat pro identifikaci datových toků. 
Hlavním rozdílem od UML je nejsou svázány s objektovým návrhem.

## DFD (Data flow diagram)

DFD představuje diagram toku dat, který ukazuje tok řady dat na základě určitého modelu informačního systému. 

DFD se obecně používá k nastínění vzoru a rámce datového systému bez zobrazování možností doby zpracování v sekvenci, například volby Ano nebo Ne v typických vývojových diagramech. 

Ve skutečnosti lze DFD použít pro správu informací nebo vizualizaci dat. Zde můžete vidět příklad DFD, který ukazuje celkový datový tok pro provedení rezervace v restauraci.

![](https://www.edrawsoft.com/erdiagram/images/dfd-diagram-example.png)

## EDR (Entitě relační diagram)
Zobrazuje vztahy mezi různými entitami v informačním systému: členy, role, položky, produkty, umístění, koncepty a další. 

Mezi nejčastější praktické využití ERD patří relační databáze. Níže je uveden dobrý příklad ERD pro zachycení vztahů ve firme.

![](https://sparxsystems.com/resources/gallery/diagrams/images/entity-relationship-data-modeling.png)

ERD existuje v několika formách. Asi nejznámější je tak, která se používá pro zachycení strukturr databáze:

![](https://sparxsystems.com/enterprise_architect_user_guide/15.2/images/physical-data-model-information-engineering.png.png)

# Eriksson-Penker
rozšiřuje UML – nabízí pohled na procesní architekturu organizace obohacenou o cíle jednotlivých procesů, zdroje a vlastníky.

![](https://sparxsystems.com/enterprise_architect_user_guide/14.0/images/epdiag.png)

---
_Většinu z výše zmíněných digramů podporuje Enterpise architekt._ 