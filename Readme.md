# ZÁKLADY UML MODELOVÁNÍ V PROSTŘEDÍ ENTERPISE ARCHITECT (UML1)

## PROGRAMOVÁNÍ, OOA, UML

Jazyk UML (Unified Modeling Language) se stal za posledních 25 letech fenoménem v oblasti analytických metod. V tomto kurzu Vám pomůžeme probojovat se houštinou různých modelů a diagramů, které UML analytikovi a návrháři nabízí.  
V rámci kurzu získáte základní přehled a schopnost, jak tyto prostředky použít při práci na SW projektech. Jednotlivé modely a diagramy budou prezentovány na jednoduchých příkladech s použitím CASE nástroje Enterprise Architect.  
V rámci kurzu předpokládáme bohatou diskuzi nad jednotlivými příklady, zaměřenou na dotazy a potřeby účastníků. Absolvování tohoto kurzu Vás určitě povzbudí k dalšímu zkoumání možností, jak Vám UML pomůže zlepšit vaše analytické vyjadřování.

## V KURZU SE NAUČÍTE

-   Získání kontextu: opakování fází SW projektu
-   Jaká je úloha analýzy v SW projektech a proč se používá modelování
-   Rychlý pohled do historie jazyka UML
-   Základní informace o těchto modelech/diagramech:
	1.  Model/Diagram tříd (Class model)
	2.  Diagram objektů (Object diagram)
	3.  Komponentní diagram (Component diagram)
	4.  Diagram balíčků (Package diagram)
	5.  Model případů užití (Use case model)
	6.  Diagram aktivit (Activity diagram)
	7.  Stavový diagram (State diagram)
	8.  Sekvenční diagram (Sequence diagram)
-   UML ve zpětném zrcátku: co s fází SW projekt je a není v UML
-   Základy práce s nástrojem pro vytváření diagramů UML Enterprise Architect
-   Doporučení dalšího vzdělávání a čtení

## KURZ JE URČENÝ PRO

-   Projektové manažery, který potřebují v přiměřené míře rozumět pracovním prostředkům pracovníků na projektu
-   IT analytiky, kteří se zatím s UML nesetkali. A někdy je snad i odradilo množství informací, které v současnosti k UML existuje.
-   Business analytiky, kteří si hodlají vylepšit a zpřesnit výrazové možnosti
-   Programátory, kteří se cítí povolání k tomu, aby byli rovnoprávní a kritičtí partneři analytiků při přebírání jejich výsledků

## Náplň kurzu:

-   [Vývoj SW systémů, vývojové fáze SW projektu, modely apod](01_Vyvoj_SW.md)
	1.  Rozdělení SW projektu na fáze/etapy, proč vlastně?
	2.  Role analýzy: na co musí dát analýza odpověď
	3.  Role modelování v analýze
	4.  Průlet historií analytických metodik
	5.  Nástroje pro podporu modelování: Computer Aided Software Engineering - CASE
-   [Historie a vznik UML, přehled diagramů, obecné prvky](02_Historie_a_vznik_UML.md)
	1.  Three amigos: Booch, Rambough, Jacobsson a další inspirativní autoři
	2.  Stručná charakteristika verzí
	3.  Srovnání s jinými analytickými metodikami
1. **Požadavky**: sběr požadavků je prvním a klíčovým krokem každého procesu vývoje.
	-   [Model případů užití](03_UseCase.md)
		1.  Role modelu v analýze
		2.  Základní prvky modelu případů užití
		3.  Příklady v EA
		4.  Vlastní práce účastníků
2. **Analýza**: 
	-   [Analytický diagram tříd, a model objektů](04_Analyticke_diagramy.md)
		1.  Role modelu v analýze
		2.  Základní prvky diagramu tříd
		3.  Základní prvky diagramu objektů
		4.  Příklady v EA
		5.  Vlastní práce účastníků
	-   [Model balíčků](05_Diagram_balicku.md)
		1.  Základní prvky diagramu balíčků
		2.  Debata o roli pro uspořádání analýzy a projektu
		3.  Ukázka v EA
	-   [Sekvenční diagram](06_Sekvencni_diagram.md)
		1.  Role modelu v analýze nebo návrhu
		2.  Základní prvky sekvenčního diagramu
		3.  Příklady v EA
	-   [Diagram aktivit](07_Diagram_aktivit.md)
		1.  Role modelu v analýze
		2.  Základní prvky diagramu aktivit
		3.  Příklady v EA
		4.  Vlastní práce účastníků
3. **Návrh**:
	-   [Model/diagram tříd, model/diagram objektů](08_Diaram_trid.md)
		1.  Role modelu v analýze
		2.  Základní prvky diagramu tříd
		3.  Základní prvky diagramu objektů
		4.  Příklady v EA
		5.  Vlastní práce účastníků
	-   [Diagram komponent](09_Diagram_komponent.md)
		1.  Role modelu v analýze
		2.  Základní prvky diagramu aktivit
		3.  Příklady v EA
	-   [Stavový diagram](10_Stavový diagram.md)
		1.  Role modelu v analýze
		2.  Základní prvky stavového diagramu
		3.  Příklady v EA
-   Závěr
	1.  Doplňující témata
	2.  Otázky účastníků
	3.  Odkazy na další informační zdroje

## Zdroje a odkazy
- Specifikace požadavků https://cw.fel.cvut.cz/old/_media/courses/a4m33sep/prednasky/02_requirements.pdf
- Část knihy UML 2 - https://adoc.pub/jim-arlow-ila-neustadt-uml-2-a-unifikovany-proces-vyvoje-apl.html
- Podobně jako [plantUML](https://plantuml.com/) je ještě [mermaid](https://mermaid.js.org/). jedná drobná výhoda, mermaid je integrován do Azure DevOps wiki, takže je možné diagramy vkládat přímo do dokumentace.





