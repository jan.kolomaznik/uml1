# Diagram aktivit
Diagram aktivity je dalším důležitým diagramem v UML pro popis dynamických aspektů systému.

Diagram činností je v podstatě vývojový diagram, který znázorňuje tok z jedné činnosti do jiné činnosti. Činnost lze popsat jako činnost systému.

Řídicí tok se převádí z jedné operace do druhé. Tento tok může být sekvenční, rozvětvený nebo souběžný. Diagramy aktivit se zabývají všemi typy řízení toku pomocí různých prvků, jako je vidlice, spojení atd

---
1.  Role modelu v analýze
2.  Základní prvky diagramu aktivit
3.  Příklady v EA
4.  Vlastní práce účastníků