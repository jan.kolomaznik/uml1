# Zadání od zákazníka
Dobrý den, máme zde projekt, na kterém bychom rádi spolupracovali s vámi. Chceme naučit lidi pít kávu po italském způsobu a vytvořit pro to komunitu kávových milovníků. Plánujeme vytvořit databázi kaváren, které budou součástí našeho projektu. Uživatelé budou moci tato místa navštěvovat, hodnotit je a doporučovat je svým přátelům.

Nechceme vytvářet chat mezi uživateli, ale chceme se zaměřit na recenze a hodnocení těchto kaváren. Po odchodu z kavárny by uživatel dostal výzvu k hodnocení a pokud by procházel kolem dobře hodnocené kavárny, rádi bychom mu ji nabídli.

Budeme potřebovat mobilní verzi, ale zatím nám stačí jen webová verze pro telefon. Z hlediska nákladů se snažíme udržet projekt co nejlevnější.

Chceme, aby aplikace fungovala globálně a byla dostupná v češtině a angličtině. Uživatelé by se měli moci registrovat přes klasické sítě jako Google nebo MS.

Potřebujeme také onboarding pro uživatele, který by ukázal, jak aplikaci používat - třeba ve formě video tutoriálu na začátku. A budeme potřebovat vývojářskou dokumentaci pro další rozvoj a nasazení aplikace.

Na závěr bych dodal, že aplikace by měla být škálovatelná a snadno rozšiřitelná. O technologické řešení necháváme volné ruce vám.