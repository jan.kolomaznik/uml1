# Pruvodce galerie

_Software pro prezentaci, rezervační systém, a **interaktivní průvodce pro vystavu obrazu**._ 

Poptávané softwarové řešení se bude skládat ze tří komponent: CMS systému (content management system) pro  prezentaci výstavy (déle jen CMS), rezervačního systému pro prodej lístků (dále jen eshop) a interaktivního průvodce (dále jen pruvodce).
CMS a eshop budou úzce propojeny do jedné webové aplikace. Původce bude mít formu mobilní aplikace.

## Původce 
(v1) Aplikace průvodce bude mobilní aplikace, kterou si bude mocí návštěvník stáhnou do svého mobilního zařízení nebo ji využívat na zapůjčeném zařízení v rámci výstavy.

Bude se jednat o interaktivní aplikaci, v rámci které se návštěvníci dozvědí atraktivní formou nejen věcné informace, ale i další zajímavosti.


### Funkční zadání
Jedná se o průvodce, kterého si náštěvník může:
1. spustit na zapůjčeném tabletu
2. spustit na vlastním tabletu nebo mobilním telefonu
3. spustit doma na PC či jiném zařízení

Verze na zapůjčeném tabletu přímo na místě a dokáže sama rozpoznat, u kterého plátna náštěvník stojí (nutno zohlednit místot a směr pohledu) a nabídnout mu příslušný komentář.
- Komentáře si člověk volí výběrem části plátna.

### Průvedce obsahuje
- textový, audio, vizální -> foto + video
- Rozlišuje věkové a cílové skupiny
	- předškolní věk
	- 1. stupeň ZŠ
	- 2. stupeň ZŠ
	- SŠ / VŠ
	- dospělý
	- umělecky orientovaní dospělý
- umožňuje volit na vizuýlu daného plátna detailní info ke zvolenému detailu, prvku či části.
- má v sobě kalenář a upozorňuje na akce naplánované pořadatelem v kalenáři nebo na webu
- umí sbírat mailové kontatky náštěvníků a zařadit je do centrální databáze (při vstupu do app a na vybraných místech popupy)
- musí mít rozhranní pro zprávu průvodnce a případnou modifikaci a aktualizaci

### Reporty
- statistická data o návšetevnícívh (vek, ...)
- doba strávená u pláten
- detily, které je zajímaly a zda je doposlouchaly nebo přešetli
- kolikrát odešli na doprovodné stránky

### Nefunkční požadavky
- musí být flexibilní a být snadno rošiřitelná
- části aplikace se budou dát modifikovovat (aprilový provoz)
- loklizovaltená do všech světových jazyků
- kód musí byt pokrýt testy minimálně ze 90% a mít obdoně kvalitní metriku
- v odbě otvírací doby musí být zabespečn proti výpadku systému
- Všechna data by měla být zálohována všetně postupu obnovy po pádu
- odezvana požadavek musí být do 0.1s při 10 000 požadavcích za sekundu
- kod aplikace musí být uložen na chráněmém repozitáři.
- Při nasazení nové verze nebo upravy, neprve projde testováním na malém vzorku náštevníků, než bude plně použita

